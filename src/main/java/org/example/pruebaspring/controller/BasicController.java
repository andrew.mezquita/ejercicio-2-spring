package org.example.pruebaspring.controller;


import org.example.pruebaspring.model.Movie;
import org.example.pruebaspring.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

//@RestController Esto devuelve objetos
@Controller //Esto devuelve vistas
public class BasicController {

    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String index(){
        //Devuelve la vista index
        return "index";
    }

    @RequestMapping("/add")
    String add(){
        //Devuelve la vista add
        return "addMovie";
    }

    @RequestMapping("/delete")
    String delete(){
        //Devuelve la vista delete
        return "delete";
    }

    @RequestMapping("/put")
    String put(){
        //Devuelve la vista delete
        return "updateMovie";
    }

    @PostMapping("/add")
    String postMovie(@RequestParam String movie_name, @RequestParam String url, Model model){

        Movie m = new Movie();
        m.setMovie_name(movie_name);
        m.setUrl(url);
        movieService.addMovie(m);
        //Ya hemos añadido la película

        model.addAttribute("message", "La película " + movie_name + " ha sido añadida.");
        return "index";
    }

    @PutMapping("/put")
    String updateMovie(@RequestParam Long id, @RequestParam String movie_name, @RequestParam String url, Model model) {
        Movie m = new Movie();
        m.setId(id);
        m.setMovie_name(movie_name);
        m.setUrl(url);

        if (movieService.getMovieById(id) == null) {
            model.addAttribute("message", "La película " + movie_name + " no ha sido actualizada.");
        } else {
            movieService.updateMovie(id,m);
            model.addAttribute("message", "La película " + movie_name + " ha sido actualizada.");
        }
        return "index";
    }

    @DeleteMapping("/delete")
    String pruebaBorrar(@RequestParam Long id, Model model) {

        if (movieService.getMovieById(id) == null) {
            model.addAttribute("message", "La película no ha sido borrada.");
        } else {
            movieService.deleteMovie(id);
            model.addAttribute("message", "La película ha sido borrada.");
        }
        return "index";
    }
}
