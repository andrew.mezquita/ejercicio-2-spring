package org.example.pruebaspring.repository;

import org.example.pruebaspring.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
