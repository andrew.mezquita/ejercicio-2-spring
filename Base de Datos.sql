CREATE DATABASE Xilften;
USE Xilften;
drop table movies;
Create table movies (
id int primary key AUTO_INCREMENT,
movie_name varchar(255),
url varchar(10000)
);
insert movies (movie_name) values ("Avatar");
insert movies (movie_name) values ("Terminator");
insert movies (movie_name) values ("Titanic");
insert movies (movie_name) values ("Aliens");
insert movies (movie_name) values ("Solaris");
insert movies (movie_name) values ("The Abyss");

select *
from movies;